package com.android.image.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.android.image.models.CompatibilityQuestion;

import java.util.List;

@Dao
public interface ImageDao {

    @Query("SELECT * FROM image_table")
    LiveData<List<CompatibilityQuestion>> getAllImages();

    @Insert
    void insert(CompatibilityQuestion data);

    @Query("DELETE FROM image_table")
    void deleteAllImages();

}
