package com.android.image.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.view.View;

import com.android.image.models.CompatibilityQuestion;
import com.android.image.models.MainData;
import com.android.image.repositories.BaseRepository;

import java.util.List;

public class MainActivityViewModel extends AndroidViewModel {

    BaseRepository repository;
    LiveData<List<CompatibilityQuestion>> data;
    LiveData<List<CompatibilityQuestion>> db_data;

    public final ObservableField<Integer> visibility =
            new ObservableField<>();

    public MainActivityViewModel(@NonNull Application application) {
        super(application);
        repository = BaseRepository.getRepository(application);
        data = repository.getImageData();
        db_data = repository.getDataFromDb();

    }


    public LiveData<List<CompatibilityQuestion>> getImages(){
        if(data != null){
            visibility.set(View.GONE);
        }
        return data;

    }

    public LiveData<List<CompatibilityQuestion>> getFromDb(){
        if(db_data != null){
            visibility.set(View.GONE);
        }
        return db_data;
    }



}
