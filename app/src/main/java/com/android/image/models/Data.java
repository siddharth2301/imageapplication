package com.android.image.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "image_table")
public class Data {
    public Data() {
    }

    @PrimaryKey
    Integer id;

    static int image ;

    public Data(int image) {
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Data{" +
                "image=" + image +
                '}';
    }


}
