package com.android.image.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class DeletionReason {
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("next_action_text")
    @Expose
    private String nextActionText;
    @SerializedName("click_action")
    @Expose
    private String clickAction;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getNextActionText() {
        return nextActionText;
    }

    public void setNextActionText(String nextActionText) {
        this.nextActionText = nextActionText;
    }

    public String getClickAction() {
        return clickAction;
    }

    public void setClickAction(String clickAction) {
        this.clickAction = clickAction;
    }
}
