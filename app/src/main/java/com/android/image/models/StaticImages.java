package com.android.image.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class StaticImages {
    @SerializedName("spark")
    @Expose
    private String spark;

    public String getSpark() {
        return spark;
    }

    public void setSpark(String spark) {
        this.spark = spark;
    }
}
