package com.android.image.models;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "image_table")
public class SampleModel {

    @PrimaryKey(autoGenerate = true)
    private Integer sreial_number;

    String sample;

    public SampleModel(String sample) {
        this.sample = sample;
    }

    public String getSample() {
        return sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }

    public Integer getSreial_number() {
        return sreial_number;
    }

    public void setSreial_number(Integer sreial_number) {
        this.sreial_number = sreial_number;
    }

    @Override
    public String toString() {
        return "SampleModel{" +
                "sample='" + sample + '\'' +
                '}';
    }
}
