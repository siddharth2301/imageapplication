package com.android.image.models;

import android.arch.persistence.room.Entity;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Style {

    public Style(){

    }

    @SerializedName("medium")
    @Expose
    private static String medium;
    @SerializedName("large")
    @Expose
    private String large;
    @SerializedName("thumb")
    @Expose
    private String thumb;
    @SerializedName("original")
    @Expose
    private String original;

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }


    // data binding for image

//    @BindingAdapter({"bind:imageUrl"})
//    public static void loadImage(ImageView view, int image_id){
//        Glide.with(view.getContext()).load(medium).into(view);
//    }

}
