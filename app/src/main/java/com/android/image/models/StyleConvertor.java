package com.android.image.models;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;

public class StyleConvertor {
    @TypeConverter
    public static Style toStyle(String timestamp) {
        return timestamp == null ? null : new Gson().fromJson(timestamp,Style.class);
    }

    @TypeConverter
    public static String styleToString(Style style){
        return  style == null ? null : new Gson().toJson(style);
    }
}
