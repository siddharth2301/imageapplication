package com.android.image.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity (tableName = "image_table")
public class CompatibilityQuestion {

        public CompatibilityQuestion(){

        }

        @PrimaryKey(autoGenerate = false)
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("question")
        @Expose
        private String question;
        @SerializedName("tick")
        @Expose
        private String tick;
        @SerializedName("cross")
        @Expose
        private String cross;

        @TypeConverters(StyleConvertor.class)
        @SerializedName("style")
        @Expose
        private Style style;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public String getTick() {
            return tick;
        }

        public void setTick(String tick) {
            this.tick = tick;
        }

        public String getCross() {
            return cross;
        }

        public void setCross(String cross) {
            this.cross = cross;
        }

        public Style getStyle() {
            return style;
        }

        public void setStyle(Style style) {
            this.style = style;
        }
}
