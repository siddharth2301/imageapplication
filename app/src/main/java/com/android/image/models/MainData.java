package com.android.image.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MainData {

        @SerializedName("compatibility_questions")
        @Expose
        private List<CompatibilityQuestion> compatibilityQuestions = null;
        @SerializedName("profile_questions")
        @Expose
        private List<ProfileQuestion> profileQuestions = null;
        @SerializedName("hiv_statuses")
        @Expose
        private List<String> hivStatuses = null;
        @SerializedName("tribes")
        @Expose
        private List<String> tribes = null;
        @SerializedName("interests")
        @Expose
        private List<String> interests = null;
        @SerializedName("orientations")
        @Expose
        private List<Orientation> orientations = null;
        @SerializedName("group_leave_reasons")
        @Expose
        private List<GroupLeaveReason> groupLeaveReasons = null;
        @SerializedName("group_report_reasons")
        @Expose
        private List<GroupReportReason> groupReportReasons = null;
        @SerializedName("user_unmatch_reasons")
        @Expose
        private List<UserUnmatchReason> userUnmatchReasons = null;
        @SerializedName("user_report_reasons")
        @Expose
        private List<UserReportReason> userReportReasons = null;
        @SerializedName("user_group_report_reasons")
        @Expose
        private List<UserGroupReportReason> userGroupReportReasons = null;
        @SerializedName("deactivation_reasons")
        @Expose
        private List<DeactivationReason> deactivationReasons = null;
        @SerializedName("deletion_reasons")
        @Expose
        private List<DeletionReason> deletionReasons = null;
        @SerializedName("static_images")
        @Expose
        private StaticImages staticImages;
        @SerializedName("share_app_text")
        @Expose
        private String shareAppText;
        @SerializedName("share_app_link")
        @Expose
        private String shareAppLink;

        public List<CompatibilityQuestion> getCompatibilityQuestions() {
            return compatibilityQuestions;
        }

        public void setCompatibilityQuestions(List<CompatibilityQuestion> compatibilityQuestions) {
            this.compatibilityQuestions = compatibilityQuestions;
        }

        public List<ProfileQuestion> getProfileQuestions() {
            return profileQuestions;
        }

        public void setProfileQuestions(List<ProfileQuestion> profileQuestions) {
            this.profileQuestions = profileQuestions;
        }

        public List<String> getHivStatuses() {
            return hivStatuses;
        }

        public void setHivStatuses(List<String> hivStatuses) {
            this.hivStatuses = hivStatuses;
        }

        public List<String> getTribes() {
            return tribes;
        }

        public void setTribes(List<String> tribes) {
            this.tribes = tribes;
        }

        public List<String> getInterests() {
            return interests;
        }

        public void setInterests(List<String> interests) {
            this.interests = interests;
        }

        public List<Orientation> getOrientations() {
            return orientations;
        }

        public void setOrientations(List<Orientation> orientations) {
            this.orientations = orientations;
        }

        public List<GroupLeaveReason> getGroupLeaveReasons() {
            return groupLeaveReasons;
        }

        public void setGroupLeaveReasons(List<GroupLeaveReason> groupLeaveReasons) {
            this.groupLeaveReasons = groupLeaveReasons;
        }

        public List<GroupReportReason> getGroupReportReasons() {
            return groupReportReasons;
        }

        public void setGroupReportReasons(List<GroupReportReason> groupReportReasons) {
            this.groupReportReasons = groupReportReasons;
        }

        public List<UserUnmatchReason> getUserUnmatchReasons() {
            return userUnmatchReasons;
        }

        public void setUserUnmatchReasons(List<UserUnmatchReason> userUnmatchReasons) {
            this.userUnmatchReasons = userUnmatchReasons;
        }

        public List<UserReportReason> getUserReportReasons() {
            return userReportReasons;
        }

        public void setUserReportReasons(List<UserReportReason> userReportReasons) {
            this.userReportReasons = userReportReasons;
        }

        public List<UserGroupReportReason> getUserGroupReportReasons() {
            return userGroupReportReasons;
        }

        public void setUserGroupReportReasons(List<UserGroupReportReason> userGroupReportReasons) {
            this.userGroupReportReasons = userGroupReportReasons;
        }

        public List<DeactivationReason> getDeactivationReasons() {
            return deactivationReasons;
        }

        public void setDeactivationReasons(List<DeactivationReason> deactivationReasons) {
            this.deactivationReasons = deactivationReasons;
        }

        public List<DeletionReason> getDeletionReasons() {
            return deletionReasons;
        }

        public void setDeletionReasons(List<DeletionReason> deletionReasons) {
            this.deletionReasons = deletionReasons;
        }

        public StaticImages getStaticImages() {
            return staticImages;
        }

        public void setStaticImages(StaticImages staticImages) {
            this.staticImages = staticImages;
        }

        public String getShareAppText() {
            return shareAppText;
        }

        public void setShareAppText(String shareAppText) {
            this.shareAppText = shareAppText;
        }

        public String getShareAppLink() {
            return shareAppLink;
        }

        public void setShareAppLink(String shareAppLink) {
            this.shareAppLink = shareAppLink;
        }

    }
