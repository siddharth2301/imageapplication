package com.android.image.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.image.R;
import com.android.image.adapters.VerticalRecyclerViewAdapter;
import com.android.image.databinding.ActivityMainBinding;
import com.android.image.models.CompatibilityQuestion;
import com.android.image.models.MainData;
import com.android.image.models.SampleModel;
import com.android.image.viewmodels.MainActivityViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import in.goodiebag.carouselpicker.CarouselPicker;
import okhttp3.Cache;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    MainActivityViewModel model;
    VerticalRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

         binding = DataBindingUtil.setContentView(this,R.layout.activity_main);

         model = ViewModelProviders.of(this).get(MainActivityViewModel.class);

         model.getImages().observe(this, new Observer<List<CompatibilityQuestion>>() {
             @Override
             public void onChanged(@Nullable List<CompatibilityQuestion> compatibilityQuestions) {
                 if (compatibilityQuestions == null){
                     getDataFromDB();
                     return;
                 }

                 Log.d("jnvwlv","here");
                 adapter = new VerticalRecyclerViewAdapter(compatibilityQuestions);

                 binding.setMyAdapter(adapter);


             }
         });

         binding.setModel(model);



    }

    private void getDataFromDB() {
        model.getFromDb().observe(this, new Observer<List<CompatibilityQuestion>>() {
            @Override
            public void onChanged(@Nullable List<CompatibilityQuestion> list) {
                Log.d("jnvwlv","heredb");
                adapter = new VerticalRecyclerViewAdapter(list);
                binding.setMyAdapter(adapter);
            }
        });
    }
}
