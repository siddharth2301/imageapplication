package com.android.image.utils;

import android.app.Application;

import com.android.image.interfaces.RetrofitInterface;
import com.bumptech.glide.request.RequestOptions;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.android.image.utils.AppConstants.BASE_URL;

public class RetrofitClass {

    static int cache_size = 5 * 1024 * 1024;

    public static final String HEADER_PRAGMA = "Pragma";
    public static final String HEADER_CACHE_CONTROL = "Cache-Control";

    private static Retrofit getInstance(){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
//                .cache(cache())
                .addInterceptor(interceptor) // data logging
//                .addNetworkInterceptor(networkInterceptor()) // only if connected
//                .addInterceptor(offlineInterceptor()) // offline
                .build();

        return new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(client).build();
    }

    public static RetrofitInterface getInterface(){
        return getInstance().create(RetrofitInterface.class);
    }

    public static Cache cache(){
        return new Cache(App.getInstance().getCacheDir(),cache_size);
    }

    private static Interceptor networkInterceptor(){
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Response response = chain.proceed(chain.request());

                CacheControl cacheControl = new CacheControl.Builder()
                                                .maxAge(5, TimeUnit.MINUTES).build();

                return response.newBuilder().removeHeader(HEADER_PRAGMA)
                        .removeHeader(HEADER_CACHE_CONTROL)
                        .header(HEADER_CACHE_CONTROL, cacheControl.toString())
                        .build();
            }
        };
    }

    private static Interceptor offlineInterceptor(){

        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Request request = chain.request();

                    CacheControl cacheControl = new CacheControl.Builder()
                            .maxStale(7, TimeUnit.DAYS)
                            .build();


                    request = request.newBuilder()
                            .removeHeader(HEADER_PRAGMA)
                            .removeHeader(HEADER_CACHE_CONTROL)
                            .cacheControl(cacheControl).cacheControl(cacheControl)
                            .build();


                return chain.proceed(request);
            }
        };
    };
}
