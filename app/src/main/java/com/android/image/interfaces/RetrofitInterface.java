package com.android.image.interfaces;

import com.android.image.models.CompatibilityQuestion;
import com.android.image.models.MainData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitInterface {

    @GET("api/v2/meta/data")
    Call<MainData> getImages();

}
