package com.android.image.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.android.image.Dao.ImageDao;
import com.android.image.models.CompatibilityQuestion;
import com.android.image.models.Data;

@Database(entities = {CompatibilityQuestion.class}, version = 1)
public abstract class ImageDataBase extends RoomDatabase {

    static ImageDataBase dataBase;
    static String DATABASE_NAME = "image_database";

    public abstract ImageDao imageDao();

    public static synchronized ImageDataBase getInstance(Context context) {
        if (dataBase == null) {
            dataBase = Room.databaseBuilder(context.getApplicationContext(),
                    ImageDataBase.class, DATABASE_NAME)
                    .build();
        }
        return dataBase;
    }
}
