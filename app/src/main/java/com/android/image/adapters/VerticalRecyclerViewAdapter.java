package com.android.image.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.android.image.R;
import com.android.image.databinding.VerticalRvViewAdapterBinding;
import com.android.image.models.CompatibilityQuestion;
import com.android.image.models.Data;


import java.util.ArrayList;
import java.util.List;

public class VerticalRecyclerViewAdapter extends RecyclerView.Adapter<VerticalRecyclerViewAdapter.ViewHolder> {

    List<CompatibilityQuestion> models = new ArrayList<>();
    Context context;

    public VerticalRecyclerViewAdapter(List<CompatibilityQuestion> models){
        this.models = models;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        VerticalRvViewAdapterBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                R.layout.vertical_rv_view_adapter,
                viewGroup,false);
        context = viewGroup.getContext();
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        CompatibilityQuestion model = models.get(i);
        viewHolder.binding.setModel(model);

        HorizontalRecyclerViewAdapter adapter = new HorizontalRecyclerViewAdapter(models);
        viewHolder.binding.setAdapter(adapter);

        RecyclerView recyclerView = viewHolder.binding.recyclerView;

        try {
            recyclerView.setOnFlingListener(null);
            LinearSnapHelper snapHelper = new LinearSnapHelper();
            snapHelper.attachToRecyclerView(recyclerView);
        }
        catch (IllegalStateException e){

        }

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);

        linearLayoutManager.scrollToPosition(Integer.MAX_VALUE / 2);

    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public VerticalRvViewAdapterBinding binding;
        public ViewHolder(@NonNull VerticalRvViewAdapterBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
        }
    }


}
