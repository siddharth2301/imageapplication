package com.android.image.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.image.R;
import com.android.image.databinding.HorizontalRvViewAdapterBinding;
import com.android.image.models.CompatibilityQuestion;
import com.bumptech.glide.Glide;

import java.util.List;

public class HorizontalRecyclerViewAdapter extends RecyclerView.Adapter<HorizontalRecyclerViewAdapter.ViewHolder> {

    List<CompatibilityQuestion> data;
    Context context;

    public HorizontalRecyclerViewAdapter(List<CompatibilityQuestion> data){
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        HorizontalRvViewAdapterBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                R.layout.horizontal_rv_view_adapter,viewGroup,false);
        context = viewGroup.getContext();
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        int real_position = i % data.size();
        CompatibilityQuestion d = data.get(real_position);
        viewHolder.binding.setData(d);

        Glide.with(context).load(d.getStyle().getLarge()).into(viewHolder.imageView);
    }


    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE / 2;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView ;
        HorizontalRvViewAdapterBinding binding;
        public ViewHolder(@NonNull HorizontalRvViewAdapterBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            imageView = binding.image;
        }
    }
}
