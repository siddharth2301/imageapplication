package com.android.image.repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.util.Log;

import com.android.image.Dao.ImageDao;
import com.android.image.database.ImageDataBase;
import com.android.image.interfaces.RetrofitInterface;
import com.android.image.models.CompatibilityQuestion;
import com.android.image.models.Data;
import com.android.image.models.MainData;
import com.android.image.utils.RetrofitClass;

import java.util.List;

import okhttp3.Cache;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseRepository {

    private static BaseRepository repository;
    private static RetrofitInterface retrofitInterface;
    ImageDao imageDao;

    private MutableLiveData<List<CompatibilityQuestion>> data = new MutableLiveData<>();

    private LiveData<List<CompatibilityQuestion>> liveData;

    public static BaseRepository getRepository(Application application){
        if (repository == null){
            repository = new BaseRepository(application);
        }
        return repository;
    }

    public BaseRepository(Application application){
        retrofitInterface = RetrofitClass.getInterface();
        ImageDataBase imageDataBase = ImageDataBase.getInstance(application);
        imageDao = imageDataBase.imageDao();
        liveData = imageDao.getAllImages();
    }

    public MutableLiveData<List<CompatibilityQuestion>> getImageData(){

        retrofitInterface.getImages().enqueue(new Callback<MainData>() {
            @Override
            public void onResponse(Call<MainData> call, Response<MainData> response) {
                data.postValue(response.body().getCompatibilityQuestions());
                if (response.isSuccessful()){
                    insertData(response.body().getCompatibilityQuestions());
                }
            }

            @Override
            public void onFailure(Call<MainData> call, Throwable t) {
                data.setValue(liveData.getValue());
            }
        });

        return data;
    }

    public void insertData(List<CompatibilityQuestion> list){
        new InsertData(imageDao,list).execute();
    }

    class InsertData extends AsyncTask<Void, Void, List<CompatibilityQuestion>>{

        ImageDao imageDao;
        List<CompatibilityQuestion> list;

        InsertData(ImageDao imageDao, List<CompatibilityQuestion> list){
            this.imageDao = imageDao;
            this.list = list;
        }

        @Override
        protected List<CompatibilityQuestion> doInBackground(Void... voids) {
            imageDao.deleteAllImages();

            for (CompatibilityQuestion question : list){
                imageDao.insert(question);
            }

            return null;
        }
    }

    public LiveData<List<CompatibilityQuestion>> getDataFromDb(){
        return liveData;
    }


}
